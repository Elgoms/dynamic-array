#pragma once
#include <utility>
#include <stdlib.h>

template <class T> 
class DynArray
{
public:

	DynArray() 
	{
		size = 0;
		capacity = 8;
		elements = nullptr;
		InitializeArray();
	}

	DynArray(size_t capacity) : capacity(capacity)
	{
		size = 0;
		InitializeArray();
	}

	~DynArray()
	{
		for (size_t i = 0; i < size; ++i)
		{
			elements[i].~T();
		}

		free(elements);
	}

	T& operator [] (size_t index)
	{
		return At(index);
	}

	void PushBack(T&& item);
	void PushBack(const T& item);

	void Reserve(size_t cap);

	T& At(size_t index) { return elements[index]; }; //let it crash if index is out of range

	template<class... Args>
	void EmplaceBack(Args&&... args);

	void Erase(T* from, T* to);

	const size_t Size() { return size; }

	T* begin() { return &elements[0]; };
	T* end() { return &elements[size]; };

private:

#define cast(to_cast) static_cast<T*>(to_cast)

	size_t capacity;
	size_t size;

	T* elements;

	void InitializeArray();
	void Reset();
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


template<class T>
inline void DynArray<T>::PushBack(T&& item)
{
	if (size < capacity)
	{
		new (&elements[size]) T(item);
		size++;
	}
	else
	{
		capacity *= 2;
		elements = cast(realloc(elements, capacity * sizeof(T)));
		new (&elements[size]) T(item);
		size++;
	}
}

template<class T>
inline void DynArray<T>::PushBack(const T& item)
{
	if (size < capacity)
	{
		new (&elements[size]) T(item);
		size++;
	}
	else
	{
		capacity *= 2;
		elements = cast(realloc(elements, capacity * sizeof(T)));
		new (&elements[size]) T(item);
		size++;
	}
}

template<class T>
inline void DynArray<T>::Reserve(size_t cap)
{
	if (cap <= capacity)
	{
		return;
	}
	else
	{
		elements = cast(malloc(cap * sizeof(T)));
	}
}

template<class T>
inline void DynArray<T>::Erase(T* from, T* to)
{
	const size_t first = from - begin();
	const size_t last = to - begin();
	const size_t range = to - from;
	const size_t new_size = size - range;

	if (range == size)
	{
		Reset();
		return;
	}

	auto buff = cast(malloc((new_size) * sizeof(T)));

	for (size_t i = 0, j = 0; i < size; ++i)
	{
		if (i < first || i >= last)
		{
			buff[j] = std::move(elements[i]);
			j++;
		}
	}

	free(elements);

	InitializeArray();

	for (size_t i = 0; i < new_size; ++i)
	{
		elements[i] = std::move(buff[i]);
	}

	free(buff);
}

template<class T>
template<class... Args>
inline void DynArray<T>::EmplaceBack(Args&&... args)
{
	if (size < capacity)
	{
		new (&elements[size]) T(std::forward<Args>(args)...);
		size++;
	}
	else
	{
		capacity *= 2;
		elements = cast(realloc(elements, capacity * sizeof(T)));
		new (&elements[size]) T(std::forward<Args>(args)...);
		size++;
	}
}

template<class T>
inline void DynArray<T>::InitializeArray()
{
	elements = cast(malloc(capacity * sizeof(T)));
}

template<class T>
inline void DynArray<T>::Reset()
{
	for (size_t i = 0; i < size; i++)
	{
		elements[i].~T();
	}

	free(elements);

	size = 0;
	capacity = 8;
	elements = nullptr;

	InitializeArray();
}
